@extends('admin.layouts.dashboard')
@push('css')
<link rel="stylesheet" href="{{ asset('backend/dist/css/bootstrap-tagsinput.css') }}">
@endpush

@section('content')

<div class="container-fluid">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="wrap-list-role">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List Role</h3>
                <a href="{{ route('roles.index') }}" class="btn btn-primary btn-sm float-right"><i class="fas fa-plus-square"></i>  Back</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form method="POST" action="{{ route('roles.store') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="role_name">Role name</label>
                        <input type="text" name="role_name" class="form-control" id="role_name" placeholder="Role name..." value="{{ old('role_name') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="role_slug">Role Slug</label>
                        <input type="text" name="role_slug" tag="role_slug" class="form-control" id="role_slug" placeholder="Role Slug..." value="{{ old('role_slug') }}" required>
                    </div>
                    <div class="form-group" >
                        <label for="roles_permissions">Add Permissions</label>
                        <input type="text" data-role="tagsinput" name="roles_permissions" class="form-control" id="roles_permissions" value="{{ old('roles_permissions') }}">
                    </div>

                    <div class="form-group pt-2">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
          <!-- /.card -->
    </div>
</div>

@endsection
@push('js')
<script src="{{ asset('backend/dist/js/bootstrap-tagsinput.js') }}"></script>

<script>

    $(document).ready(function(){
        $('#role_name').keyup(function(e){
            var str = $('#role_name').val();
            str = str.replace(/\W+(?!$)/g, '-').toLowerCase();//rplace stapces with dash
            $('#role_slug').val(str);
            $('#role_slug').attr('placeholder', str);
        });
    });

</script>
@endpush

