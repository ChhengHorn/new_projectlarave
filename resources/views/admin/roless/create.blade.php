@extends('admin.layouts.dashboard')
@push('css')
<link rel="stylesheet" href="{{ asset('backend/dist/css/bootstrap-tagsinput.css') }}">
@endpush

@section('content')

<div class="container-fluid">
    {{-- @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif --}}
    <div class="wrap-list-role">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List manage Roles</h3>
                <a href="{{ route('roless.index') }}" class="btn btn-primary btn-sm float-right"><i class="fas fa-plus-square" ></i>  Back</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form method="POST" action="{{ route('roless.store') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Manage Roles Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="manage roles name..." value="{{ old('name') }}" >
                    </div>
                    <hr>
                    <h4>permission</h4>
                    @foreach($manage_controllers as $item)
                        <div class="row">
                            <div class="col-md-6">
                                {{ $item->name }}
                            </div>
                            <div class="col-md-6">
                                <?php
                                $filtered = collect($actions)->where('controller_id', $item->id)->all();
                                ?>
                                @foreach($filtered as $action)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="checkbox" name="permission[{{ $action->id }}]"
                                            id="permission_[{{ $action->id }}]" class="status">
                                            <label for="permission_[{{ $action->id }}]" class="roleActionlabel">{{ $action->name }}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                    @endforeach
                    <hr>
                    <div class="form-group">
                        <label>
                            <input type="checkbox" id="status" class="status" name="status" checked>
                            Publish
                        </label>
                    </div>
                    <div class="form-group pt-2">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
          <!-- /.card -->
    </div>
</div>

@endsection
@push('js')
<script src="{{ asset('backend/dist/js/bootstrap-tagsinput.js') }}"></script>

<script>

    $(document).ready(function(){
        $('#permission_name').keyup(function(e){
            var str = $('#permission_name').val();
            str = str.replace(/\W+(?!$)/g, '-').toLowerCase();//rplace stapces with dash
            $('#permission_slug').val(str);
            $('#permission_slug').attr('placeholder', str);
        });
    });

</script>
@endpush

