@extends('admin.layouts.dashboard')
@section('title')
    Manage Rolss
@endsection
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/toastr/css/toastr.min.css') }}">
@endpush
@section('content')
    <div class="container-fluid">
        <div class="wrap-list-role">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List Manage Controller</h3>
                    <a href="{{ route('roless.create') }}" class="btn btn-primary btn-xs float-right"><i class="fas fa-plus-square"></i>  NEW</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Role Name</th>
                      <th>Created By</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($roless as $key => $roles)

                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $roles['name'] }}</td>
                        <td>
                            {{ $roles->created_name }}
                        </td>
                        <td>
                            <a href="{{ route('roless.edit', $roles->id) }}" class="btn btn-info btn-xs edit"><i class="fas fa-edit"></i> Edit</a>
                            <a href="javascript:deleteObject({{ $roles->id }})" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"> Delete</i></a>
                                <form id="frmDeleteObject-{{ $roles->id }}" style="display: none" action="{{ route('roless.destroy',$roles->id) }}" role="form" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Role Name</th>
                        <th>Created By</th>
                        <th>Action</th>
                        </tr>
                    </tfoot>
                  </table>

                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
        </div>
    </div>
@endsection
@push('js')
<!-- DataTables -->
<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/sweetalert2/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('backend/sweetalert2/js/sweetalert2@10.js') }}"></script>
<script src="{{ asset('backend/toastr/js/toastr.min.js') }}"></script>
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
</script>
<script type="text/javascript">
    function deleteObject(id){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('frmDeleteObject-'+id).submit();
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        }
      })
    }
</script>

@if(Session::has('success'))
    <script>
        toastr.success("{!! Session::get('success') !!}");
    </script>
@endif
@if(Session::has('warning'))
    <script>
        toastr.info("{!! Session::get('warning') !!}");
    </script>
@endif
@endpush

