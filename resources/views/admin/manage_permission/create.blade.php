@extends('admin.layouts.dashboard')
@push('css')
<link rel="stylesheet" href="{{ asset('backend/dist/css/bootstrap-tagsinput.css') }}">
@endpush

@section('content')

<div class="container-fluid">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="wrap-list-role">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List manage Permission</h3>
                <a href="{{ route('manage_permission.index') }}" class="btn btn-primary btn-sm float-right"><i class="fas fa-plus-square"></i>  Back</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form method="POST" action="{{ route('manage_permission.store') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Manage Controler Name</label>
                        <select class="form-control select2" style="width: 100%;" name="controller_id">
                            <option value="">Select Manage Permission</option>
                            @foreach ($manageCons as $manageCon)
                                <option value="{{ $manageCon->id }}">
                                    {{ $manageCon->name }}
                                </option>
                            @endforeach
                        </select>
                      </div>
                    <div class="form-group">
                        <label for="name">Manage Permission Name</label>
                        <input type="text" name="permission_name" class="form-control" id="permission_name" placeholder="manage permission name..." value="{{ old('permission_name') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="role_slug">Manage Permission Slug</label>
                        <input type="text" name="permission_slug" tag="permission_slug" class="form-control" id="permission_slug" placeholder="manage permission  Slug..." value="{{ old('permission_slug') }}" required>
                    </div>
                    <div class="form-group pt-2">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
          <!-- /.card -->
    </div>
</div>

@endsection
@push('js')
<script src="{{ asset('backend/dist/js/bootstrap-tagsinput.js') }}"></script>

<script>

    $(document).ready(function(){
        $('#permission_name').keyup(function(e){
            var str = $('#permission_name').val();
            str = str.replace(/\W+(?!$)/g, '-').toLowerCase();//rplace stapces with dash
            $('#permission_slug').val(str);
            $('#permission_slug').attr('placeholder', str);
        });
    });

</script>
@endpush

