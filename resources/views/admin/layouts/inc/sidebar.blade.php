<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">
            @auth
            {{ Auth::user()->name }} | {{ Auth::user()->roles->isNotEmpty() ? Auth::user()->roles->first()->name : "" }}
            @endauth
        </a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Manage Teacher
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('teacher.index') }}" class="nav-link">
                    <i class="fa fa-plus-circle nav-icon"></i>
                    <p>Teacher</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>
                    Manage Roless
                    <i class="fas fa-angle-left right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('roless.index') }}" class="nav-link">
                    <i class="fa fa-plus-circle nav-icon"></i>
                    <p>Roless</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Manage Post
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('posts.index') }}" class="nav-link">
                    <i class="fa fa-plus-circle nav-icon"></i>
                    <p>Post</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Manage Role
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                @can('isAdmin')
                    <li class="nav-item">
                        <a href="{{ route('roles.index') }}" class="nav-link">
                        <i class="fa fa-plus-circle nav-icon"></i>
                        <p>Role</p>
                        </a>
                    </li>
                @endcanany
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Manage User
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                @canany(['isAdmin','isManager'])
                    <li class="nav-item">
                        <a href="{{ route('user.index') }}" class="nav-link">
                        <i class="fa fa-plus-circle nav-icon"></i>
                        <p>User</p>
                        </a>
                    </li>
                @endcanany
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-users-cog"></i>
              <p>
                Manage Permission
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="{{ route('manage_controller.index') }}" class="nav-link">
                  <i class="fa fa-plus-circle nav-icon"></i>
                  <p>Manage Controller</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('manage_permission.index') }}" class="nav-link">
                  <i class="fa fa-plus-circle nav-icon"></i>
                  <p>Manage Permission</p>
                </a>
              </li>
            </ul>
        </li>
        <li class="nav-header">EXAMPLES</li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">

              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>@lang('home.logout')</p>
          </a>
        </li>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
          @csrf
      </form>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
