@extends('admin.layouts.dashboard')
@push('css')
<link rel="stylesheet" href="{{ asset('backend/dist/css/bootstrap-tagsinput.css') }}">
@endpush

@section('content')

<div class="container-fluid">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="wrap-list-role">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List manage Controller</h3>
                <a href="{{ route('manage_controller.index') }}" class="btn btn-primary btn-sm float-right"><i class="fas fa-plus-square"></i>  Back</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form method="POST" action="{{ route('manage_controller.store') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Manage Controller</label>
                        <input type="text" name="manage_name" class="form-control" id="manage_name" placeholder="manage name..." value="{{ old('name') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="role_slug">Role Slug</label>
                        <input type="text" name="manage_slug" tag="manage_slug" class="form-control" id="manage_slug" placeholder="manage  Slug..." value="{{ old('manage_slug') }}" required>
                    </div>
                    <div class="form-group pt-2">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
          <!-- /.card -->
    </div>
</div>

@endsection
@push('js')
<script src="{{ asset('backend/dist/js/bootstrap-tagsinput.js') }}"></script>

<script>

    $(document).ready(function(){
        $('#manage_name').keyup(function(e){
            var str = $('#manage_name').val();
            str = str.replace(/\W+(?!$)/g, '-').toLowerCase();//rplace stapces with dash
            $('#manage_slug').val(str);
            $('#manage_slug').attr('placeholder', str);
        });
    });

</script>
@endpush

