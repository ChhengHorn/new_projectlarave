@extends('admin.layouts.dashboard')
@section('title')
    Manage Controller
@endsection
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/toastr/css/toastr.min.css') }}">
@endpush
@section('content')
    <div class="container-fluid">
        <div class="wrap-list-role">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List Manage Controller</h3>
                    <a href="{{ route('manage_controller.create') }}" class="btn btn-primary btn-xs float-right"><i class="fas fa-plus-square"></i>  NEW</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>

                      <th>Created By</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($manageCon as $key => $manage)

                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $manage['name'] }}</td>
                        <td>
                            {{ $manage->created_name }}
                        </td>
                        <td>

                            <a href="{{ route('manage_controller.edit', $manage->id) }}" class="btn btn-danger btn-xs edit"><i class="fas fa-edit"></i> Edit</a>
                            <a href="#" class="btn btn-info btn-xs delete" data-toggle="modal" data-target="#deleteModal" data-roleid="{{$manage['id']}}">
                            <i class="fas fa-trash-alt"> Delete</i></a>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                      <th>Name</th>

                      <th>Created By</th>
                      <th>Action</th>
                    </tr>
                    </tfoot>
                  </table>
                  <!-- delete Modal-->
                  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">
                          <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Are you shure you want to delete this?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                          </button>
                          </div>
                          <div class="modal-body">Select "delete" If you realy want to delete this role.</div>
                          <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <form method="POST" action="">
                              @method('DELETE')
                              @csrf
                              {{-- <input type="hidden" id="role_id" name="role_id" value=""> --}}
                              <a class="btn btn-primary" onclick="$(this).closest('form').submit();">Delete</a>
                          </form>
                          </div>
                      </div>
                      </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
        </div>
    </div>
@endsection
@push('js')
<!-- DataTables -->
<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/toastr/js/toastr.min.js') }}"></script>
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
</script>

<script>
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var role_id = button.data('roleid')

        var modal = $(this)
        // modal.find('.modal-footer #role_id').val(role_id)
        modal.find('form').attr('action','/manage_controller/' + role_id);
    })
</script>

@if(Session::has('success'))
    <script>
        toastr.success("{!! Session::get('success') !!}");
    </script>
@endif
@if(Session::has('warning'))
    <script>
        toastr.info("{!! Session::get('warning') !!}");
    </script>
@endif
@endpush

