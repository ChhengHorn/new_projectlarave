@extends('admin.layouts.dashboard')
@section('title')
    Teacher
@endsection
@section('teacher','active')
@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/toastr/css/toastr.min.css') }}">
@endpush
@section('content')
    <div class="container-fluid">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Teacher Informartion</h3>
              @cannot('isManager')
              <a href="{{ route('teacher.create') }}" class="btn btn-primary pull-right btn-xs"><i class="fa fa-plus-square"></i> NEW</a>
              @endcannot
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>PROFILE</th>
                  <th>TEACHER NAME</th>
                  <th>CREATED BY</th>
                  <th>DOB</th>
                  <th>PHONE NUMBER</th>
                  <th>STATUS</th>
                  <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($teacher as $key => $item)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>
                                <img src="{{ $item->profile == ''? asset('images/noimage.png'):asset('uploads/teachers/'.$item->profile) }}" width="35px" height="35px" alt="">
                            </td>
                            <td>{{ $item->name }}</td>
                             {{-- <td>{{ $item->created_by }}</td>  --}}
                            <td>{{ $item->created_name}}</td>
                            <td>{{ $item->dob }}</td>
                            <td>{{ $item->phone_num }}</td>
                            <td>
                                @if ($item->status==1)
                                    <label for="active" class="label label-success"> Active</label>
                                @else()
                                    <label for="active" class="label label-danger"> Inactive</label>
                                @endif
                            </td>
                            <td>

                                <a href="{{ route('teacher.show',$item->id) }}" class="btn btn-primary btn-xs edit"><i class="fa fa-eye"></i> view</a>
                                {{--  @can('edit', $item)  --}}
                                <a href="{{ route('teacher.edit',$item->id) }}" class="btn btn-primary btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                {{--  @endcan  --}}
                                <a href="javascript:deleteObject({{ $item->id }})" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"> Delete</i></a>
                                <form id="frmDeleteObject-{{ $item->id }}" style="display: none" action="{{ route('teacher.destroy',$item->id) }}" role="form" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
  <!-- /.box -->
@endsection
@push('js')
<!-- DataTables -->
<script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/sweetalert2/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('backend/sweetalert2/js/sweetalert2@10.js') }}"></script>
<script src="{{ asset('backend/toastr/js/toastr.min.js') }}"></script>

<script>
    $(function () {
      $('#example1').DataTable()
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
</script>
<script type="text/javascript">
    function deleteObject(id){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('frmDeleteObject-'+id).submit();
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        }
      })
    }
</script>
@if(Session::has('success'))
    <script>
        toastr.success("{!! Session::get('success') !!}");
    </script>
@endif
@if(Session::has('warning'))
    <script>
        toastr.info("{!! Session::get('warning') !!}");
    </script>
@endif
@endpush
