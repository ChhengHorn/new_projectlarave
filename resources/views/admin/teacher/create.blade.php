@extends('admin.layouts.dashboard')
@section('title')
    Add Teacher
@endsection
@section('teacher','active')

@push('css')
<link rel="stylesheet" href="{{ asset('backend/bower_components/select2/dist/css/select2.min.css') }}">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <style>
      .profile{
          text-align: center;
      }
  </style>
@endpush
@section('content')
  <div class="container-fluid">
    <div class="box">
        <div class="box-header">
          <h3 class="box-title">Teacher Informartion</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form role="form" action="{{ route('teacher.store') }}" method="POST" enctype="multipart/form-data" >
                @csrf
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Teacher Name</label>
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-user-plus"></i>
                                      </div>
                                      <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
                                      <font style="color: red">{{ ($errors->has('name'))?($errors->first('name')):'' }}</font>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                      </div>
                                      <input type="number" min="0" class="form-control" name="phone_num" id="phone_num" placeholder="Enter phone number">
                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div class="form-group">
                                    <label>DOB</label>
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" name="dob" class="form-control pull-right date" placeholder="Enter dob">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="profile">
                            <img id="preview" src="{{ asset('images/noimage.png') }}" width="150px" height="130px"/><br/>
                            <input type="file" id="image" name="image" style="display: none;"/>
                            <br>
                            <label for="button" class="btn btn-primary btn-xs">
                            <a style="color: white" href="javascript:changeProfile()" >Upload</a>
                            </label>
                            <label for="button" class="btn btn-danger btn-xs">
                                <a style="color: white" href="javascript:removeImage()" id="remove">Remove</a>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea id="description" name="description"></textarea>
                </div>
                <div class="form-group">
                    <label>
                        <input type="checkbox" id="status" class="status" name="status" checked>
                        Publish
                    </label>
                </div>
                <div class="box-footer">
                    <a href="{{ route('teacher.index') }}" class="btn btn-danger pull-left btn-xs"><i class="fa fa-backward"></i> Back</a>
                    <button type="submit" class="btn btn-primary pull-right btn-xs"> <i class="fa fa-save "></i> SAVE</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
@endsection
@push('js')
<script src="{{ asset('backend/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
    });
    $(document).ready(function(){
        $('#description').summernote();
         //Date picker
        $('.date').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy'
        })
    });
</script>
<script>
    function changeProfile() {
        $('#image').click();
    }
    $('#image').change(function () {
        var imgPath = this.value;
        var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
            readURL(this);
        else
            alert("Please select image file (jpg, jpeg, png).")
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            };
               $("#remove").val(0);
        }
    }
    function removeImage() {
    // $('#preview').attr('src',"{{ asset('images/noimage.jpg') }}");
	// $('#preview').attr('src', '');
       $('#preview').attr('src', '{{ asset('images/noimage.png') }}');
           $("#remove").val(1);
    }
</script>
@endpush
