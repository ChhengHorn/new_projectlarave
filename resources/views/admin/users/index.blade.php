@extends('admin.layouts.dashboard')
@section('title')
    User
@endsection
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush
@section('content')
    <div class="container-fluid">
        <div class="wrap-list-role">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List Role</h3>
                    <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm float-right"><i class="fas fa-plus-square"></i>  NEW</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Role</th>
                      <th>Permission</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $key => $user)
                    @if(!\Auth::user()->hasRole('admin') && $user->hasRole('admin')) @continue; @endif
                    <tr {{ Auth::user()->id == $user->id ? 'bgcolor=#ddd' : '' }}>
                        <td>{{ ++$key }}</td>
                        <td>{{ $user['name'] }}</td>
                        <td>{{ $user['email'] }}</td>
                        <td>
                            @if($user->roles->isNotEmpty())
                                @foreach($user->roles as $role)
                                    <span class="badge badge-success">
                                        {{ $role->name }}
                                    </span>
                                @endforeach
                            @endif
                        </td>
                        <td>
                            @if($user->permissions->isNotEmpty())
                                @foreach($user->permissions as $permission)
                                    <span class="badge badge-danger">
                                        {{ $permission->name }}
                                    </span>
                                @endforeach
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('user.show', $user->id) }}" class="btn btn-info btn-xs view"><i class="fas fa-eye"></i></a>
                            {{-- @can('edit', $user) --}}
                            <a href="{{ route('user.edit', $user->id) }}" class="btn btn-danger btn-xs edit"><i class="fas fa-edit"></i></a>
                            {{-- @endcan --}}
                            <a href="#" class="btn btn-info btn-xs delete" data-toggle="modal" data-target="#deleteModal" data-userid="{{$user['id']}}">
                            <i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Permission</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                  </table>
                  <!-- delete Modal-->
                  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">
                          <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Are you shure you want to delete this?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                          </button>
                          </div>
                          <div class="modal-body">Select "delete" If you realy want to delete this user.</div>
                          <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <form method="POST" action="">
                              @method('DELETE')
                              @csrf
                              {{-- <input type="hidden" id="user_id" name="user_id" value=""> --}}
                              <a class="btn btn-primary" onclick="$(this).closest('form').submit();">Delete</a>
                          </form>
                          </div>
                      </div>
                      </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
        </div>
    </div>
@endsection
@push('js')
<!-- DataTables -->
<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
</script>

<script>
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var user_id = button.data('userid')

        var modal = $(this)
        // modal.find('.modal-footer #user_id').val(user_id)
        modal.find('form').attr('action','/user/' + user_id);
    })
</script>

@if(Session::has('success'))
    <script>
        toastr.success("{!! Session::get('success') !!}");
    </script>
@endif
@if(Session::has('warning'))
    <script>
        toastr.info("{!! Session::get('warning') !!}");
    </script>
@endif
@endpush

