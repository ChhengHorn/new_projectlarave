<?php

use App\Http\Controllers\ManageController;
use App\Http\Controllers\ManagePermissionController;
use App\Http\Controllers\ManagesController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\TeacherController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('dashboards', function () {
    return view('admin.layouts.dashboard');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('posts', PostsController::class);
Route::resource('user', UserController::class)->middleware('role:admin,manager');
Route::resource('roles', RoleController::class)->middleware('can:isAdmin');

Route::resource('teacher', TeacherController::class);
Route::resource('manage_controller', ManagesController::class);
Route::resource('manage_permission', ManagePermissionController::class);
Route::resource('roless', RolesController::class);

