<?php

namespace App\Http\Controllers;

use App\Models\Roless;
use App\Models\ManageCon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ManagePermission;
use App\Models\Roles;
use App\Models\RolessPermission;

class RolesController extends Controller
{

    public function index()
    {

        $data['roless'] = Roless::leftjoin('users as u','u.id','=','rolesses.created_by')
        ->select('rolesses.*','u.name as created_name')
        ->get();

        return view('admin.roless.index', $data);
    }


    public function create()
    {
        $data['manage_controllers'] = ManageCon::all();
        $data['actions'] = ManagePermission::all();
        return view('admin.roless.create',$data);
    }


    public function store(Request $request)
    {
        if($request->status !=''){
            $status = 1;
        }else{
            $status = '0';
        }

        $data = [
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'status' => $status,
            'created_by' => auth()->user()->id,
        ];
        $roless = Roless::create($data);
        $role_id = $roless->id;
        $permission_id = $request->permission;
        foreach($permission_id as $key => $val){
            $datax = [
                'role_id' => $role_id,
                'permission_id' => $key
            ];
            RolessPermission::create($datax);
        }
        return redirect()->route('roless.index');
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $data['manage_controllers'] = ManageCon::all();
        $data['actions'] = ManagePermission::all();
        $data['role_permission'] = RolessPermission::where('role_id', $id)->get();
        $data['roless'] = Roless::find($id);
        return view('admin.roless.edit',$data);
    }


    public function update(Request $request, $id)
    {
        if($request->status !=''){
            $status = 1;
        }else{
            $status = '0';
        }
        $roless = Roless::findOrfail($id);
        $data = [
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'status' => $status,
            'created_by' => auth()->user()->id,
            'updated_at' => date("Y-m-d h:i:sa")
        ];

        $roless->update($data);
        $permission_id = $request->permission;
        RolessPermission::where('role_id', $id)->delete();
        if($permission_id){
            foreach($permission_id as $key => $val){
                $datax = [
                    'role_id' => $id,
                    'permission_id' => $key
                ];
                RolessPermission::create($datax);
            };
        }
        return redirect()->route('roless.index');
    }


    public function destroy($id)
    {
        $roless = Roless::findOrfail($id);
        if($roless->delete()){
            RolessPermission::where('role_id', $id)->delete();
            return redirect()->back();
        }
    }
}
