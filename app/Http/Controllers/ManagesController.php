<?php

namespace App\Http\Controllers;

use App\Models\ManageCon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ManagesController extends Controller
{
    public function index()
    {

            $manageCon['manageCon'] =ManageCon::leftjoin('users as u','u.id','=','manage_cons.created_by')
            ->select('manage_cons.*','u.name as created_name')
            ->get();


       return view('admin.manage_controller.index',$manageCon);
    }


    public function create()
    {
        return view('admin.manage_controller.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'manage_name' => 'required|max:255',
            'manage_slug' => 'required|max:255'
        ]);
        $data = [
            'name'          => $request->manage_name,
            'slug'          => $request->manage_slug,
            'created_by'    => auth()->user()->id
        ];
        $manageCon = ManageCon::create($data);

        return redirect()->route('manage_controller.index')->with('success','Manage add successfully!');
    }

    public function edit( $id)
    {
        // $this->authorize('edit', $id);
        $data['manageCon'] = ManageCon::find($id);
        return view('admin.manage_controller.edit',$data);
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'manage_name' => 'required|max:255',
            'manage_slug' => 'required|max:255'
        ]);
        $manageCon = ManageCon::findorfail($id);

        $data = [
            'name'          => $request->manage_name,
            'slug'          => $request->manage_slug,
            'created_by'    => auth()->user()->id
        ];
            $manageCon->update($data);
            return redirect()->route('manage_controller.index')->with('warning','manage has been update successfully!');
    }


    public function destroy($id)
    {

        $manageCon = ManageCon::findOrfail($id);
        $manageCon->delete();
        return redirect()->back();
    }
}
