<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\permission;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    public function index()
    {
        $roles = Role::orderBy('id', 'desc')->get();
        return view('admin.roles.index',['roles' => $roles]);
    }


    public function create()
    {
        return view('admin.roles.create');
    }


    public function store(Request $request)
    {
        //validate the role fields
        $request->validate([
            'role_name' => 'required|max:255',
            'role_slug' => 'required|max:255'
        ]);

        $role = new Role();
        $role->name = $request->role_name;
        $role->slug = $request->role_slug;
        $role->save();

        $listOfPermissions = explode(',', $request->roles_permissions);//create array from separated/coma permissions

        foreach ($listOfPermissions as $permission) {
            $permissions = new permission();
            $permissions->name = $permission;
            $permissions->slug = strtolower(str_replace(" ", "-", $permission));
            $permissions->save();
            $role->permissions()->attach($permissions->id);
            $role->save();
        }
        return redirect()->route('roles.index');
    }


    public function show(Role $role)
    {
        return view('admin.roles.view', ['role' => $role]);
    }


    public function edit(Role $role)
    {
        return view('admin.roles.edit', ['role' => $role]);
    }


    public function update(Request $request, Role $role)
    {
        //validate the role fields
        $request->validate([
            'role_name' => 'required|max:255',
            'role_slug' => 'required|max:255'
        ]);

        $role->name = $request->role_name;
        $role->slug = $request->role_slug;
        $role->save();

        $role->permissions()->delete();
        $role->permissions()->detach();

        $listOfPermissions = explode(',', $request->roles_permissions);//create array from separated/coma permissions

        foreach ($listOfPermissions as $permission) {
            $permissions = new Permission();
            $permissions->name = $permission;
            $permissions->slug = strtolower(str_replace(" ", "-", $permission));
            $permissions->save();
            $role->permissions()->attach($permissions->id);
            $role->save();
        }
        return redirect()->route('roles.index');
    }


    public function destroy(Role $role)
    {
        $role->permissions()->delete();
        $role->delete();
        $role->permissions()->detach();
        return redirect('/roles');
    }
}
