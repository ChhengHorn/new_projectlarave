<?php

namespace App\Http\Controllers;

use App\Models\ManageCon;
use App\Models\ManagePermission;
use Illuminate\Http\Request;

class ManagePermissionController extends Controller
{
    public function index()
    {

        $data['managepermissions'] =ManagePermission::leftjoin('users as u','u.id','=','manage_permissions.created_by')
        ->select('manage_permissions.*','u.name as created_name')
        ->get();
        $data['manageCons'] = ManageCon::all();

       return view('admin.manage_permission.index',$data);
    }


    public function create()
    {
        $ManageCons['manageCons'] =ManageCon::all();

        return view('admin.manage_permission.create', $ManageCons);
    }


    public function store(Request $request)
    {
        $request->validate([
            'controller_id' => 'required|max:255',
            'permission_name' => 'required|max:255',
            'permission_slug' => 'required|max:255'
        ]);

        $data = [
            'controller_id' => $request->controller_id,
            'name'          => $request->permission_name,
            'slug'          => $request->permission_slug,
            'created_by'    => auth()->user()->id
        ];

        $managepermission = ManagePermission::create($data);

        return redirect()->route('manage_permission.index')->with('success','Manage Permission add successfully!');
    }

    public function edit( $id)
    {
        $data['managepermissions'] = ManagePermission::find($id);
        $data['manageCons'] = ManageCon::get();
        return view('admin.manage_permission.edit',$data);
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'controller_id' => 'required|max:255',
            'permission_name' => 'required|max:255',
            'permission_slug' => 'required|max:255'
        ]);
        $managepermissions = ManagePermission::find($id);

        $data = [
            'controller_id' => $request->controller_id,
            'name'          => $request->permission_name,
            'slug'          => $request->permission_slug,
            'created_by'    => auth()->user()->id
        ];
            $managepermissions->update($data);
            return redirect()->route('manage_permission.index')->with('warning','permission has been update successfully!');
    }


    public function destroy($id)
    {
        $managepermissions = ManagePermission::findOrfail($id);
        $managepermissions->delete();
        return redirect()->back();
    }
}
