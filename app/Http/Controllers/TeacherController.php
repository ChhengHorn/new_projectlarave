<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{
    public function index()
    {
        // return sum(1,2);
        if(!checkPermission("view","teachercontroller")){
            abort(403, 'Not Permission');
        };


            $teachers['teacher'] =Teacher::leftjoin('users as u','u.id','=','teachers.created_by')
            ->select('teachers.*','u.name as created_name')
            ->get();

       return view('admin.teacher.index',$teachers);
    }


    public function create()
    {
        if(!checkPermission("create","teachercontroller")){
            abort(403, 'Not Permission');
        };
        return view('admin.teacher.create');
    }


    public function store(Request $request)
    {

        if($request->status){
            $status = 1;
        }else{
            $status = 0;
        }
        if ($request->hasFile('image')) {
            $image=$request->file('image');
            $ext = $image->getClientOriginalExtension();
            $image_name = $request->name.'-'.rand().'-'.time().'.'.$ext;
             $destinition=public_path('/uploads/teachers/');
             if(!is_dir('$destinition')){
                 mkdir('$destinition',0777,true);
             }
            $image->move($destinition,$image_name);
         } else {
             $image_name = null;
         }
        $data = [
            'name'          => $request->name,
            'profile'       => $image_name,
            'slug'          => Str::slug($request->name),
            'phone_num'     => $request->phone_num,
            'dob'           => $request->dob?date('Y-m-d',strtotime($request->dob)):NULL,
            'description'   => $request->description,
            'status'        => $status,
            'created_by'    => auth()->user()->id
        ];
        $teacher = Teacher::create($data);
        return redirect()->route('teacher.index')->with('success','Class add successfully!');
    }


    public function show(Request $request, Teacher $teacher)
    {

        return view('admin.posts.show', ['post'=>$teacher]);
    }

    public function edit( $id)
    {
        if(!checkPermission("edit","teachercontroller")){
            abort(403, 'Not Permission');
        };
        // $this->authorize('edit', $id);
        $data['teacher'] = Teacher::find($id);
        return view('admin.teacher.edit',$data);
    }


    public function update(Request $request, Teacher $teacher)
    {

        if(!checkPermission("update","teachercontroller")){
            abort(403, 'Not Permission');
        };

        $teachers = Teacher::findorfail($teacher);

        if($request->status){
            $status = 1;
        }else{
            $status = 0;
        }

        $current_date = date("Y-m-d h:i:sa");
        $old_image = $teachers->profile;
        if ($request->hasFile('image')) {
            if($old_image){
             $old_iamge_path = public_path('uploads/teachers/'.$old_image);
             if(file_exists($old_iamge_path)){
                unlink($old_iamge_path);
             }
            }
           $image=$request->file('image');
           $ext = $image->getClientOriginalExtension();
           $image_name = $request->name.'-'.rand().'-'.time().'.'.$ext;
            $destinition=public_path('/uploads/teachers/');
            if(!is_dir('$destinition')){
                mkdir('$destinition',0777,true);
            }
           $image->move($destinition,$image_name);
        } else {
            $image_name = $old_image;
        }
        $data = [
            'name'          => $request->name,
            'profile'       => $image_name,
            'slug'          => Str::slug($request->name),
            'phone_num'     => $request->phone_num,
            'dob'           => $request->dob?date('Y-m-d',strtotime($request->dob)):NULL,
            'description'   => $request->description,
            'status'        => $status,
            'created_by'    => auth()->user()->id
        ];
            $teacher = Teacher::findOrfail($teacher)->update($data);
            return redirect()->route('teacher.index')->with('warning','Teacher has been update successfully!');
    }


    public function destroy($id)
    {
        if(!checkPermission("delete","teachercontroller")){
            abort(403, 'Not Permission');
        };
        $teacher = Teacher::findOrfail($id);
        $old_image = $teacher->image;
        if($old_image){
            $old_iamge_path = public_path('uploads/teachers/'.$old_image);
            if(file_exists($old_iamge_path)){
               unlink($old_iamge_path);
            }
           }
        $teacher->delete();
        return redirect()->back();
    }
}
