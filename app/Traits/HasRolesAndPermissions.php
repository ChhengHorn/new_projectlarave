<?php

namespace App\Traits;

use App\Models\permission;
use App\Models\Role;

trait HasRolesAndPermissions
{

    public function isAdmin()
    {
        if($this->roles->contains('slug', 'admin')){
            return true;
        }
    }
    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class,'users_roles');
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(permission::class,'users_permission');
    }

    // public function hasRole($role)
    // {
    //     if( $this->roles->contains('slug', $role) ){//check if this is an list of roles

    //         return true;
    //     }
    //     return false;
    // }

    public function hasRole($role)
    {
        if( strpos($role, ',') !== false ){//check if this is an list of roles

            $listOfRoles = explode(',',$role);

            foreach ($listOfRoles as $role) {
                if ($this->roles->contains('slug', $role)) {
                    return true;
                }
            }
        }else{
            if ($this->roles->contains('slug', $role)) {
                return true;
            }
        }

        return false;
    }

}
