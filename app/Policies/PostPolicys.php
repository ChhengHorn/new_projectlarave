<?php

namespace App\Policies;

use App\Models\Teacher;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicys
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function viewAny(User $user)
    {
        //
    }


    public function view(User $user, Teacher $teacher)
    {
        //
    }


    public function create(User $user)
    {
        if ($user->roles->contains('slug', 'content-editor')) {
            return true;
        }elseif($user->permissions->contains('slug', 'create')){
            return true;
        }
        return false;
    }


    public function update(User $user, Teacher $teacher)
    {
        if($user->roles->contains('slug', 'content-editor')){
            return true;
        } elseif($user->permissions->contains('slug', 'edit')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Teacher $teacher)
    {
        if($user->permissions->contains('slug', 'delete')) {
            return true;
        } elseif ($user->roles->contains('slug', 'content-editor')) {
            return true;
        }
        return false;
    }


    public function restore(User $user, Teacher $teacher)
    {
        //
    }


    public function forceDelete(User $user, Teacher $teacher)
    {
        //
    }
}
