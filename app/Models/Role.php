<?php

namespace App\Models;

use App\Models\permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends Model
{
    use HasFactory;

    public function permissions()
    {
        return $this->belongsToMany(permission::class, 'roles_permissions');
    }
    public function allRolePermissions()
    {
        return $this->belongsToMany(permission::class, 'roles_permissions');
    }
}
