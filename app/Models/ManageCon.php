<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManageCon extends Model
{
    use HasFactory;
    protected $table = 'manage_cons';
    protected $fillable = [
        'name','slug','created_by'
    ];
}
