<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManagePermission extends Model
{
    use HasFactory;
    protected $table = 'manage_permissions';
    protected $fillable = [
        'name','slug','created_by','controller_id'
    ];
}
