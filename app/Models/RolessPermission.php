<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolessPermission extends Model
{
    use HasFactory;
    protected $table = 'roless_permissions';
    protected $fillable = [
        'role_id','permission_id'
    ];
}
