<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roless extends Model
{
    use HasFactory;
    protected $table = 'rolesses';
    protected $fillable = [
        'name','slug','created_by','status'
    ];
}
