<?php

use App\Models\Roless;
use Illuminate\Support\Facades\DB;

// function checkPermission($permission_slug,$controller_slug){
//     $role_id = auth()->user()->role;
//     $role_name = Roless::where('id',$role_id)->select('slug as slug')->first();
//     $role_slug = $role_name->slug;
//     $check = DB::select("SELECT rp.id FROM roless_permissions as rp INNER JOIN rolesses as r ON r.id = rp.role_id
//     INNER JOIN manage_permissions as mp ON mp.id = rp.permission_id
//     INNER JOIN manage_cons as mc ON mc.id = mp.controller_id
//     WHERE r.slug = '$role_slug' AND mp.slug = '$permission_slug' AND mc.slug =​ '$controller_slug'");
//     if($check){
//         $result = 1;
//     }else{
//         $result = 0;
//     }
//     return $result;

// }
function sum($a,$b){
    return $a + $b;
}
function checkPermission($permission_slug,$controller_slug){
    $role_id = auth()->user()->role;
    $role_name = Roless::where('id',$role_id)->select('slug as slug')->first();
    $role_slug = $role_name->slug;
    $check = DB::select("SELECT rp.id,mc.slug,mp.slug,r.slug FROM roless_permissions as rp INNER JOIN rolesses as r ON r.id = rp.role_id
    INNER JOIN manage_permissions as mp ON mp.id = rp.permission_id
    INNER JOIN manage_cons as mc ON mc.id = mp.controller_id
    WHERE r.slug = '$role_slug' AND mp.slug = '$permission_slug' AND mc.slug = '$controller_slug'");
    if($check){
        $result = 1;
    }else{
        $result = 0;
    }
    return $result;
}
